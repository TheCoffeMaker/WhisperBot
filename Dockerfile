FROM python:3.7-buster
MAINTAINER thecoffemaker <thecoffemaker@riseup.net>

ENV DEBIAN_FRONTEND noninteractive
ENV BOTVERSION 2
RUN apt update && apt upgrade -y

ARG JID
ARG PASS
ARG ROOM
ARG NICK
ENV QUOTES_FILE="quotes.txt"
ENV ABOUT_FILE="about.txt"
ENV TZ="America/Argentina/Buenos_Aires"
ENV WHISPER_MODEL="base"

RUN apt install -y python3 python3-pip ffmpeg tzdata

WORKDIR /bot

COPY requirements.txt .
RUN pip3 install -r requirements.txt

COPY whisperbot.py .
COPY ${QUOTES_FILE} .
COPY ${ABOUT_FILE} .

VOLUME /root/.cache/

ENTRYPOINT python3 /bot/whisperbot.py --jid ${JID} --password ${PASS} --room ${ROOM} --nick ${NICK} --quotes ${QUOTES_FILE} --about ${ABOUT_FILE} --model ${WHISPER_MODEL}
