# WhisperBot

XMPP bot that transliterates audio messages using OpenAI's Whisper libraries

## Running Whisperbot

As KISS as it can get

```
git clone https://codeberg.org/TheCoffeMaker/WhisperBot.git
cd WhisperBot
docker build -t whisperbot .
docker docker run -e JID="bot_acct@your.xmpp.server" \
                  -e PASS="a_very_secure_password" \
                  -e ROOM="that_room_with_audio_messages@muc.at.some.server" \
                  -e NICK="Bot_Nick" \
                  -v /host/volume/path/:/root/.cache/ \
                  --name whisperbot \
                  whisperbot:latest

```

Or with `docker-compose`:
```
git clone https://codeberg.org/TheCoffeMaker/WhisperBot.git
cd Whisperbot
vi bot.env    # Edit bot parameters
docker-compose up
```

Or directly downloading from DockerHub:
```
docker docker run -e JID="bot_acct@your.xmpp.server" \
                  -e PASS="a_very_secure_password" \
                  -e ROOM="that_room_with_audio_messages@muc.at.some.server" \
                  -e NICK="Bot_Nick" \
                  -v /host/volume/path/:/root/.cache/ \
                  --name whisperbot \
                  thecoffemaker/whisperbot:latest
```

Container's volume can be omited, but will download the Whisper's model each time
it runs. By using volume we avoid to download the model each time the container
get restarted.

You can see it working or reach us at [Cyberdelia's Lost in Cyberspace channel](xmpp:lostincyberspace@conference.cyberdelia.com.ar?join).
