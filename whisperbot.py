#!/usr/bin/env python3

# Slixmpp: The Slick XMPP Library
# Copyright (C) 2010  Nathanael C. Fritz
# This file is part of Slixmpp.
# See the file LICENSE for copying permission.

import logging
from getpass import getpass
from argparse import ArgumentParser

import slixmpp
from slixmpp.exceptions import IqError, IqTimeout
import asyncio
import whisper
import re
import os
import urllib.request
import time
import random



class WhisperBot(slixmpp.ClientXMPP):

    """
    A simple XMPP that transliterates audios with Whisper
    """

    def __init__(self, jid, password, room, nick, quotes, about, model):
        slixmpp.ClientXMPP.__init__(self, jid, password)

        self.room = room
        self.nick = nick

        self.quotes = quotes
        self.about = about
        self.model = model

        self.add_event_handler("session_start", self.start)
        self.add_event_handler("groupchat_message", self.muc_message)
        self.schedule('WhisperBone',15,self.keep_alive, repeat=True)
        self.add_event_handler("disconnected",self.disconnected)

    async def start(self, event):
        """
        Process the session_start event.
        """
        await self.get_roster()
        self.send_presence()
        self.plugin['xep_0045'].join_muc(self.room,
                                         self.nick,
                                         # If a room password is needed, use:
                                         # password=the_room_password,
                                         )

    def reconnect_on_future_exception(self, fut):
        if fut.exception() is not None:
            if type(fut.exception()) is IqTimeout:
                logging.warning("Connection timeout...")
                self.reconnect()
            elif type(fut.exception()) is IqError:
                logging.warning("IQ Stanza Error %s" % (fut.exception(),))
                self.reconnect()
            else:
                logging.error("Future exception %s" % (fut.exception(),))

    def keep_alive(self):
        """
        Sends a ping to keep connection alive
        """
        try:
            loop = asyncio.get_running_loop()
        except RuntimeError:  # 'RuntimeError: There is no current event loop...'
            loop = None

        if loop and loop.is_running():
            tsk = loop.create_task(self['xep_0199'].ping(self.boundjid.bare,timeout=10))
            tsk.add_done_callback(lambda t: self.reconnect_on_future_exception(t))

    def disconnected(self):
        """
        Handles disconnection by reconnecting
        """
        logging.info("reconnecting...")
        self.reconnect()


    def muc_message(self, msg):
        """
        Process incoming message stanzas from any chat room. Be aware
        that if you also have any handlers for the 'message' event,
        message stanzas may be processed by both handlers, so check
        the 'type' attribute when using a 'message' event handler.

        Whenever the bot's nickname is mentioned, respond to
        the message.

        IMPORTANT: Always check that a message is not from yourself,
                   otherwise you will create an infinite loop responding
                   to your own messages.

        This handler will reply to messages that mention
        the bot's nickname.

        Arguments:
            msg -- The received message stanza. See the documentation
                   for stanza objects and the Message stanza to see
                   how it may be used.
        """
        print("Got a message")
        if self.is_audio_msg(msg):
            logging.info('Got an audio from %s' % msg['mucnick'])
            try:
                loop = asyncio.get_running_loop()
            except RuntimeError:  # 'RuntimeError: There is no current event loop...'
                loop = None

            if loop and loop.is_running():
                print('Async event loop already running. Adding coroutine to '\
                      'the event loop.')
                tsk = loop.create_task(self.sendtranscription(msg))
                tsk.add_done_callback(
                        lambda t: print(f'Task done with result={t.result()}'))
        elif msg['mucnick'] != self.nick and self.nick in msg['body'] \
                               and "about" in msg['body']:
                self.send_message(mto=msg['from'].bare,
                                  mbody="%s" % self.abouttext(self.about),
                                  mtype='groupchat')
        elif msg['mucnick'] != self.nick and self.nick in msg['body']:
                self.send_message(mto=msg['from'].bare,
                                  mbody="%s" % self.randomtext(self.quotes),
                                  mtype='groupchat')

        print("Done")


    def abouttext(self, file_name):
        """
        """
        retval = "Number 5... is alive! and here to help with audio transliteration 🤖️"
        with open(file_name, 'r') as file:
            retval = file.readlines()
        return "🤖️ %s" % ' '.join(retval)


    def randomtext(self, file_name):
        """
        """
        retval = "Ops!"
        with open(file_name, 'r') as file:
            textlines = file.readlines()
            retval = random.choice(textlines)
        return "🤖️ %s" % retval


    async def sendtranscription(self, msg):
        """
        Threadable method
        """
        rcv_time = time.strftime("%H:%M", time.localtime())
        audio_msg_file = self.get_audio(msg['body'])
        transcript = await self.transcribe(audio_msg_file)
        self.send_message(mto=msg['from'].bare,
                          mbody="💬 A las %s hrs., %s dijo: %s" \
                                  % (rcv_time, msg['mucnick'], transcript),
                          mtype='groupchat')
        self.rm_audio(audio_msg_file)
        logging.info('Transcription sent')


    def is_audio_msg(self, msg):
        """
        True or False depending on if stanza has an audio message url
        """
        body = msg['body']
        match = re.match(r'^https?://.+\.(m4a|mp3|ogg|opus)$', body)
        if not match:
            return False
        audio_url = match.group(0)

        # Check if the URL points to a valid audio file
        if audio_url.endswith(('.mp3', '.m4a', 'ogg', 'opus')):
            logging.info('Got an audio url!')
            return True

        return False


    def get_audio(self, url):
        """
        Downloads the audio file
        """
        # Set the directory to save the downloaded file in
        save_directory = "/srv/downloads"

        # Create the directory if it doesn't exist
        if not os.path.exists(save_directory):
            os.makedirs(save_directory)

        # Get the filename from the URL
        filename = os.path.basename(url)

        # Combine the save directory and filename to create the full file path
        file_path = os.path.join(save_directory, filename)

        # Download the file from the URL and save it to the specified path
        urllib.request.urlretrieve(url, file_path)
        logging.info('File %s downloaded' % file_path)
        return file_path


    def rm_audio(self, file_path):
        """
        Deletes the audio file
        """
        try:
            os.remove(file_path)
            print(f"{file_path} has been deleted successfully!")
        except OSError as e:
            print(f"Error: {file_path} - {e.strerror}.")


    async def transcribe(self, audio_file):
        """
        Returns audio transcription text
        """
        model = whisper.load_model(self.model)
        result = model.transcribe(audio_file)
        logging.info('File %s transliterated' % audio_file)
        # returns the recognized text
        # return "[%s] %s" % (result["language"], result["text"])
        return result["text"]



if __name__ == '__main__':
    # Setup the command line arguments.
    parser = ArgumentParser()

    # Output verbosity options.
    parser.add_argument("-q", "--quiet", help="set logging to ERROR",
                        action="store_const", dest="loglevel",
                        const=logging.ERROR, default=logging.INFO)
    parser.add_argument("-d", "--debug", help="set logging to DEBUG",
                        action="store_const", dest="loglevel",
                        const=logging.DEBUG, default=logging.INFO)

    # JID and password options.
    parser.add_argument("-j", "--jid", dest="jid",
                        help="JID to use")
    parser.add_argument("-p", "--password", dest="password",
                        help="password to use")
    parser.add_argument("-r", "--room", dest="room",
                        help="MUC room to join")
    parser.add_argument("-n", "--nick", dest="nick",
                        help="MUC nickname")
    parser.add_argument("-u", "--quotes", dest="quotes",
                        help="quotes text file")
    parser.add_argument("-a", "--about", dest="about",
                        help="about text file")
    parser.add_argument("-m", "--model", dest="model",
                        help="Whisper model")

    args = parser.parse_args()

    # Setup logging.
    logging.basicConfig(level=args.loglevel,
                        format='%(levelname)-8s %(message)s')

    if args.jid is None:
        args.jid = input("Username: ")
    if args.password is None:
        args.password = getpass("Password: ")
    if args.room is None:
        args.room = input("MUC room: ")
    if args.nick is None:
        args.nick = input("MUC nickname: ")

    # Setup the MUCBot and register plugins. Note that while plugins may
    # have interdependencies, the order in which you register them does
    # not matter.
    xmpp = WhisperBot(args.jid, args.password, args.room,
                      args.nick, args.quotes, args.about, args.model)
    xmpp.register_plugin('xep_0030') # Service Discovery
    xmpp.register_plugin('xep_0045') # Multi-User Chat
    xmpp.register_plugin('xep_0199') # XMPP Ping

    # Connect to the XMPP server and start processing XMPP stanzas.
    xmpp.connect()
    asyncio.get_event_loop().run_forever()
